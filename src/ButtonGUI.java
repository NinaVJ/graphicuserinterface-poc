import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonGUI implements ActionListener {

    private int clickCount = 0;
    private JFrame jFrame;
    private JLabel jLabel;
    private JPanel jPanel;


    public ButtonGUI(){

        jFrame = new JFrame();

        JButton jButton = new JButton("Don't click");
        //make the button take in the number of clicks
        jButton.addActionListener(this);
        jLabel = new JLabel("Don't do it!");

        jPanel = new JPanel();
        //create border object
        jPanel.setBorder(BorderFactory.createEmptyBorder(40, 40, 20, 40));
        jPanel.setLayout(new GridLayout(0,1));
        jPanel.add(jButton);
        jPanel.add(jLabel);

        //regular setup
        jFrame.add(jPanel, BorderLayout.CENTER);//add panel to frame
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//set what happens when frame is closed
        jFrame.setTitle("My first GUI");//set title of window
        jFrame.pack();//set window to match a size
        jFrame.setVisible(true);//set window to be visible
    }

    public static void main(String[] args) {
        new ButtonGUI();
    }

    //to increase the number of counts
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        clickCount++;
        jLabel.setText("Mischief Score: " + clickCount);
    }
}
